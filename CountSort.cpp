#include "CountSort.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <list>

using namespace std;

const int size = 200;
const int sizenrep = 10000;

void CreateFile(int n) {
    char temp[12];
    char temp2[12];
    sprintf(temp, "%d" "%s", n, ".txt");
    sprintf(temp2, "%d" "%s", n,"reply.txt");
    ofstream out(temp);
    ofstream outrep(temp2);
    for (int i=0; i<n; i++) {outrep << (rand()%size) << " ";}
    for (int i=0; i<n; i++) {out << (rand()%sizenrep) << " ";}
    out.close();
    outrep.close();
}

void SimpleSort (int n) {
    int C[size];
    int A[n];
    int count = 0;
    char temp[12];
    char sortstr[20];
    sprintf(temp, "%d" "%s", n, "reply.txt");
    sprintf(sortstr, "%d" "%s", n, "sort.txt");
    ifstream in(temp);
    ofstream out(sortstr);
    for (int i=0; i < size; i++) {
        C[i] =0;
    }
    for (int i=0; i<n; i++) {
        in >> A[i];
        C[A[i]] = C[A[i]] + 1;
        count++;
    }
    int b=0;
    for (int j=0; j < size; j++) {
        for (int i=0; i < C[j]; i++, b++) {
            A[b] = j;
            count++;
        }
    }
    for (int i=0; i<n; i++) out << A[i] << " ";
    cout << "���������� ������������ �������� ��������� ���������� ���������: " << count << endl;
}

void AdvancedSort(int n) {
    int C[n];
    int A[n];
    int B[n];
    int count;
    char temp[12];
    char sortstr[20];
    sprintf(temp, "%d" "%s", n, ".txt");
    sprintf(sortstr, "%d" "%s", n, "sortAdvance.txt");
    ifstream in(temp);
    ofstream out(sortstr);
    for (int i=0; i<n; i++) {in >> A[i]; C[i] = 0;}
    for (int i=0; i<n-1; i++) {
        for (int j=i+1; j<n; j++) {
            if (A[i] >= A[j]) {
                C[i]++;
            } else {
                C[j]++;
            }
            count++;
        }
    }
    for (int i=0; i<n; i++) {
        B[C[i]] = A[i];
        count++;
    }
    for (int i=0;i<n; i++) {
        out << B[i] << " ";
    }
    cout << "���������� ������������ ��������� ��������� ���������� ���������: " << count << endl;
}
