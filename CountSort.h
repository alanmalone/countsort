#ifndef COUNTSORT_H_INCLUDED
#define COUNTSORT_H_INCLUDED


void CreateFile(int);
void SimpleSort(int);
void AdvancedSort(int);


#endif // COUNTSORT_H_INCLUDED
